# 深度循环神经网络

到目前为止，我们只讨论了具有一个单向隐藏层的循环神经网络。 其中，隐变量和观测值与具体的函数形式的交互方式是相当随意的。 只要交互类型建模具有足够的灵活性，这就不是一个大问题。 然而，对于一个单层来说，这可能具有相当的挑战性。 之前在线性模型中，我们通过添加更多的层来解决这个问题。 而在循环神经网络中，我们首先需要确定如何添加更多的层， 以及在哪里添加额外的非线性，因此这个问题有点棘手。

事实上，我们可以将多层循环神经网络堆叠在一起， 通过对几个简单层的组合，产生了一个灵活的机制。 特别是，数据可能与不同层的堆叠有关。 例如，我们可能希望保持有关金融市场状况 （熊市或牛市）的宏观数据可用， 而微观数据只记录较短期的时间动态

![../_images/deep-rnn.svg](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/deep-rnn.svg)