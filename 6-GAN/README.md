# 6. 生成对抗网络GAN

<font color=red size=5>文章链接</font>

~~~bash
https://gitee.com/fakerlove/deep-learning
~~~

> [论文地址](https://arxiv.org/abs/1406.2661)

常用于数据生成或非监督式学习应用的**生成对抗网络(简称GAN)**

## 6.1 概念



### 6.1.1 概念

### 6.1.2 用途

除了深度学习外，有一种新兴的网络称为强化学习(Reinforcement Learning)，其中一种很具有特色的网络为生成式对抗网络(GAN)。

GAN的应用相关论文成长幅度相当大(如下图)。

![img](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/v2-1d1369066b6c4144bb4e2a4606098a5b_720w-16300387959081.jpg)

这里不详述GAN的理论或实作方式，而是探讨GAN实际应用的场域。深度学习领域最需要的是数据，但往往不是所有应用都可以收集到大量数据，并且数据也需要人工进行标注，这是非常消耗时间及人力成本。图像数据可以通过旋转、裁切或改变明暗等方式增加数据量，但如果数据还是不够呢？目前有相当多领域透过GAN方法生成非常近似原始数据的数据，例如3D-GAN就是可以生成高质量3D对象。当然，比较有趣的应用例如人脸置换或表情置换。(如下图)

![img](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/v2-7952b4cbc33e7212e90993e7170af86a_720w.jpg)



另外，SRGAN (Super Resolution GAN)可用于提高原始图像的分辨率，将作为低分辨率影像输入进GAN模型，并生成较高画质的影像(如下图)。这样的技术可整合至专业绘图软件中，协助设计师更有效率完成设计工作。

![img](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/v2-5f808f4f8d3c3fe1b5d429c4e5d26210_720w.jpg)

NVIDIA也有提供一些基于GAN的平台的应用，包含透过GauGAN网络，仅需绘制简单的线条，即可完成漂亮的画作，并且还能随意修改场景的风格(如下图)。

![img](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/v2-5e99ebb642bd46f3d7c2b8d7546e4a28_720w.jpg)



# 7. 常见的神经网络

## 5.1 RBF网络



## 5.2 SOM网络



## 5.3 ART网络



## 5.4 级联相关网络



## 5.5 Elman网络



## 5.6 Boltzmann机

