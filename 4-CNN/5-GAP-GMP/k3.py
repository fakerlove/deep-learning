import torch
from torch import nn

a = torch.randn(2, 2)

b = torch.randn(2, 3)

out = torch.mul(a, b)
print(out.shape)
