import torch
from torch import nn

m = nn.AdaptiveMaxPool2d(1)
input = torch.randn(1, 64, 8, 9)
output = m(input)
print(output.shape)


