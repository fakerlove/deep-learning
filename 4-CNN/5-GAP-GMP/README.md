# 全局平均池化GAP,全局最大池化GMP

`全局平均池化`是在论文Network in Network中提出的

* 思想：对于输出的每一个通道的特征图的所有像素计算一个`平均值`，经过全局平均池化之后就得到一个`维度`=$C_{in}$=`类别数`的特征向量，然后直接输入到softmax层

* 作用

  `代替`全连接层，可接受`任意尺寸的图像`

* 优点

  1)可以更好的将类别与最后一个卷积层的特征图对应起来(每一个通道对应一种类别，这样每一张特征图都可以看成是该类别对应的类别置信图)

  2)降低参数量，全局平均池化层没有参数，可防止在该层过拟合

  3）整合了全局空间信息，对于输入图片的spatial translation更加鲁棒



全局平均池化的作用和优点：



## 1. AvgPool2d()

 模型为

~~~python
class torch.nn.AvgPool2d(kernel_size,stride=None,padding=0,ceil_mode=False,count_include_pad=True)
~~~

其中参数为

* kernel_size(`int`或者`tuple`)-池化窗口大小
* stride(`int` 或者`tuple`,`optional`)-maxpoling的窗口移动的步长，默认值是`kernel_size`
* padding(`int` 或者`tuple`,`optional`)-输入的每一条边补充0的层数
* dilation(`int`或者`tuple`，optional)-一个控制窗口中元素步幅分参数
* ceil_mode-如果为`True`，计算输出信号大小的时候，会使用向上取整，代替默认的向下取整的操作
* count_include_pad 如果为`True`，计算平均池化时，将包括`padding`填充的0

输出大小的公式
$$
output=\frac{input+2*padding-kernel\_size}{stride}+1
$$




两者都是做二维的`平均池化`，但是它们关注的参数却不一样。

使用AvgPool2d时，需要自己计算`kernel_size` 、`stride` 与 `padding` 三个参数。才能输出正确的`x,y`.而`AdaptiveAvgPool2d`只需要输入自己想要的输出。其中的参数自己会计算

## 2. AdaptiveAvgPool2d

nn.AdaptiveAvgPool2d就是自适应平均池化，制定输出$(H,W)$

~~~python
import torch
from torch import nn

m = nn.AdaptiveAvgPool2d((5,1))
input = torch.randn(1, 64, 8, 9)
output = m(input)
print(output.shape)
~~~

结果为

~~~python
torch.Size([1, 64, 5, 1])
~~~





## 3. AdaptiveMaxPool2d

~~~python
import torch
from torch import nn

m = nn.AdaptiveMaxPool2d(1)
input = torch.randn(1, 64, 8, 9)
output = m(input)
print(output.shape)
~~~

结果

~~~python
torch.Size([1, 64, 1, 1])
~~~

