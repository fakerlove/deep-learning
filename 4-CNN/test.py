import numpy

import numpy as np
from csv import reader

# numpy读取csv文件
filepath = r'C:\Users\Joker\Desktop\2.csv'

# 前提条件是这里的数据都得是一种类型的
# csv文件数据之间是以','分割，unpack是可选参数，默认为False，表示不转置所读取的数据矩阵
# skiprows=1 表示跳过第一行，,usecols=(1,2,3,4,5)表示读取列数
# t1 = np.loadtxt(filepath, delimiter=',', skiprows=1, usecols=(0, 1, 2), encoding='UTF-8')
# print(t1)

with open(filepath, 'rt', encoding='GBK') as raw_data:
    readers = reader(raw_data, delimiter=',')
    x = list(readers)
    data = np.array(x)
    print(data)
    print(data.shape)


x=data[1:,2].astype(float)
print(x)