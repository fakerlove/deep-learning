import numpy as np
import matplotlib.pyplot as plt

# 训练集
train_data = []

# 测试集
test_data = []


def init():
    global train_data
    global test_data
    '''
    初始化数据
    :return: 无返回值
    '''
    # 加载数据
    data = np.loadtxt("Iris.txt", delimiter=',', dtype=np.float64)
    # 算出数据一共有多少行
    N = data.shape[0]

    train_N = int(N * 0.75)
    # 前75%的数据为训练集
    train_data = data[0:train_N]
    # 剩下的25%数据为测试集
    test_data = data[train_N:]


def draw(w, b):
    plt.title("感知机模型")  # 设置x
    plt.rcParams["font.sans-serif"] = ["SimHei"]
    plt.rcParams["axes.unicode_minus"] = False
    red_x = []
    red_y = []
    orange_x = []
    orange_y = []
    data = train_data
    n = data.shape[0]
    for i in range(n):
        y_i = data[i][2]
        if y_i < 0:
            red_x.append(data[i][0])
            red_y.append(data[i][1])
        else:
            orange_x.append(data[i][0])
            orange_y.append(data[i][1])

    # scatter绘制散点图
    plt.scatter(orange_x, orange_y, marker='o', color="orange")
    plt.scatter(red_x, red_y, marker='x', color="red")
    plt.xlabel("鸢尾花长度")  # 设置x轴
    plt.ylabel("鸢尾花宽度")  # 设置y轴
    x_1 = -b / w[0]
    x_2 = -b / w[1]
    plt.xlim(np.min(data.T[0]) - 1, np.max(data.T[0]) + 1)
    plt.ylim(np.min(data.T[1]) - 1, np.max(data.T[1]) + 1)
    # y = [np.matmul(w, np.transpose(x_1)) + b, np.matmul(w, np.transpose(x_2)) + b]
    plt.axline([x_1, 0], [0, x_2], linewidth=1, color="blue")

    plt.show()
    print("w---", w, "b----", b)
    print("超平面为(%.3fx_1)+(%.3fx_2)+%.3f=0" % (w[0], w[1], b))


def train(w, b, eta):
    '''
    训练
    :param w: w值，最初的值为[0,0]
    :param b: b值，最初的值为0
    :param eta: 为学习率，值为0.01
    :return:
    '''
    mistake = True
    data = train_data
    n = data.shape[0]
    while mistake:
        mistake = False
        for i in range(n):
            x_i = np.array([data[i][0], data[i][1]])
            y_i = data[i][2]
            if y_i * (np.matmul(w, np.transpose(x_i)) + b) <= 0:
                w = w + eta * y_i * x_i
                b = b + eta * y_i
                mistake = True
    return w, b

    # 开始训练


def test(w, b):
    """
    开始测试
    :param w:  训练好的w值
    :param b:  训练好的b值
    :return: 无返回值
    """
    data = test_data
    n = data.shape[0]
    error = 0
    for i in range(n):
        x_i = np.array([data[i][0], data[i][1]])
        y_i = data[i][2]
        # 计算错误
        if y_i * (np.matmul(w, np.transpose(x_i)) + b) <= 0:
            error += 1

    print("错误率为", error / n)


if __name__ == '__main__':
    # 初始化参数
    init()
    # 赋予他们初始值
    w = np.array([0, 0])
    b = 0
    eta = 0.01
    # 开始训练
    w, b = train(w, b, eta)
    # 开始测试
    test(w, b)
    # 开始画画
    draw(w, b)
