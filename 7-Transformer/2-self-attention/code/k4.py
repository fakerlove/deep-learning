import torch
from einops import rearrange

x = torch.randn(size=(1, 3, 224, 224))
output_tensor = rearrange(x, 'b  c h w  -> b h w c')
print(output_tensor.shape)
