import numpy as np
import torch
from einops import rearrange
from torch import nn


class Self_Attn(nn.Module):
    """ Self attention Layer"""

    def __init__(self, in_dim, activation=None):
        super(Self_Attn, self).__init__()
        # self.chanel_in = in_dim
        # self.activation = activation

        self.query_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim // 8, kernel_size=1)
        self.key_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim // 8, kernel_size=1)
        self.value_conv = nn.Conv2d(in_channels=in_dim, out_channels=in_dim, kernel_size=1)
        self.gamma = nn.Parameter(torch.zeros(1))

        self.softmax = nn.Softmax(dim=-1)  #

    def forward(self, x):
        """
            inputs :
                x : input feature maps( B X C X W X H)
            returns :
                out : self attention value + input feature
                attention: B X N X N (N is Width*Height)
        """
        # batch,通道数，宽，高
        m_batchsize, C, width, height = x.size()  # [1, 16, 32, 32]

        # 步骤1, 通过conv 得出q,k
        q = self.query_conv(x).view(m_batchsize, -1, width * height).permute(0, 2,
                                                                             1)  # B X CX(N) torch.Size([1, 1024, 2])
        k = self.key_conv(x).view(m_batchsize, -1, width * height)  # B X C x (*W*H) torch.Size([1, 2, 1024])
        # 步骤1, 计算得出v
        v = self.value_conv(x).view(m_batchsize, -1, width * height)  # B X C X N  torch.Size([1, 16, 1024])

        # 步骤2,  矩阵的乘法 ,q,k进行相乘,得出特征图。
        # [batch_size,1024,2]*[batch_size,2,1024]
        energy = torch.bmm(q, k)  # transpose check [1, 1024, 1024]
        # 特征图attention map，通过softmax
        attention = self.softmax(energy)  # BX (N) X (N)  torch.Size([1, 1024, 1024])

        # 步骤3,v * 特征图= 注意力
        # [1,16,1024]  *  [1,1024,1024]= torch.Size([1, 16, 1024])
        out = torch.bmm(v, attention.permute(0, 2, 1))  # torch.Size([1, 16, 1024])

        # 重新resize
        out = out.view(m_batchsize, C, width, height)  # torch.Size([1, 16, 32, 32])

        # 加上残差
        out = self.gamma * out + x
        return out, attention


if __name__ == '__main__':
    # 这个通道数需要 是8的倍数。因为 q,k,v 是使用conv算出的。 输出通道需要大于 8
    x = torch.randn(size=(1, 16, 32, 32))
    model = Self_Attn(16)
    out, attention = model(x)
    print(out.shape)
    print(attention.shape)
