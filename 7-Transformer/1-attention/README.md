# 7-1 注意力机制  初识

## 1. 简介

假设有一天热爱绘画的你决定去户外写生，你来到一片山坡上，极目远去，心旷神怡。头顶一片蔚蓝，脚踩一席草绿，远处山川连绵，眼前花草送香，暖阳含羞云后，轻风拂动衣襟，鸟啼虫鸣入耳，美景丹青共卷。



![img](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/v2-15ab76807fd0029ee40884480797e68d_720w.jpg)

你集中精神，拿起画笔将蓝天、白云、青草等等这些元素，按照所思所想纷纷绘入画板。在绘画的过程中，你会持续地关注你构思到画板上的元素（比如蓝天，白云），而不会太多关注那些其他的元素，比如风，虫鸣，阳光等等。即你的精神是聚焦在你关心的那些事物上，这其实就是注意力的体现，这种有意识的聚焦被称为**聚焦式注意力（Focus Attention**）。

然而，正当你在画板上忘我倾洒的时侯，突然有人在背后喊你的名字，你立马注意到了，然后放下画笔，转头和来人交谈。这种无意识地，往往由外界刺激引发的注意力被称为**显著性注意力（Saliency-Based Attention）**。

但不论哪一种注意力，其实都是让你在某一时刻将注意力放到某些事物上，而忽略另外的一些事物，这就是**注意力机制（Attention Mechanism）**。

在深度学习领域，模型往往需要接收和处理大量的数据，然而在`特定的某个时刻`，往往只有`少部分的某些数据是重要的`，这种情况就非常适合**Attention机制**发光发热。

`关注自己想关注的东西`



## 2. 分类

深度学习中的注意力机制通常可分为三类：软注意（全局注意）、硬注意（局部注意）和自注意（内注意）

### 2.1 软性注意力

Soft/Global Attention(软注意机制)：对每个输入项的分配的权重为0-1之间，也就是某些部分关注的多一点，某些部分关注的少一点，因为对大部分信息都有考虑，但考虑程度不一样，所以相对来说计算量比较大。

![在这里插入图片描述](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/20200531193825344.png)

我们采用一种“软性”的信息选择机制对输入信息进行汇总，其选择的信息是所有输入信息在注意力分布下的期望。
$$
att(x,q)=\sum_{i=1}^N\alpha_ix_i=E_{z\sim p(z|X,q)}[X]
$$
**注意：** 上面这个公式，被称为软性注意力机制（Soft Attention Mechanism）。

### 2.2 硬性注意力

Hard/Local Attention(硬注意机制)：对每个输入项分配的权重非0即1，和软注意不同，硬注意机制只考虑那部分需要关注，哪部分不关注，也就是直接舍弃掉一些不相关项。优势在于可以减少一定的时间和计算成本，但有可能丢失掉一些本应该注意的信息。

**实现方式1：** 选取最高概率的输入信息
$$
att(X,q)=x_j,j=argmax_{i=1}^N\alpha_i
$$
**实现方式2：** 在注意力分布式上随机采样



**缺点：** 基于最大采样或随机采样的方式来选择信息。因此最终的损失函数与注意力分布之间的函数关系不可导，**无法使用在反向传播算法进行训练**。为了使用反向传播算法，一般使用软性注意力来代替硬性注意力。

### 2.3 自注意

对每个输入项分配的权重取决于输入项之间的相互作用，即通过输入项内部的"表决"来决定应该关注哪些输入项。和前两种相比，在处理很长的输入时，具有并行计算的优势。

自注意力机制往往采用**查询-键-值**（**Query-Key-Value**）的模式。

![在这里插入图片描述](https://resource-joker.oss-cn-beijing.aliyuncs.com/picture/20200531195053411.png)

## 3. 变种



### 3.1 多头注意力

在Transformer中提出来的一种模型





参考资料

>[注意力机制 & 自注意力模型 - 知乎 (zhihu.com)](https://zhuanlan.zhihu.com/p/431441795)