# DNN

DNN是指深度神经网络。与RNN循环神经网络、CNN卷积神经网络的区别就是DNN特指全连接的神经元结构，并不包含卷积单元或是时间上的关联。

[A Gentle Introduction to Graph Neural Networks (distill.pub)](https://distill.pub/2021/gnn-intro/)